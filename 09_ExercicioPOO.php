<?php

//protected - somente na classe  pai e nas classes filhas
//public - acesso em qualquer lugar
//private - somente na classe

abstract class Conta
{
    private $tipoDaConta;
    public function getTipoDaConta()
    {
        return $this-> tipoDaConta;
    }
    public function setTipoDaConta(string $tipoDaConta)
    {
        $this-> tipoDaConta = $tipoDaConta;
    }

    public $agencia;
    public $conta;
    protected $saldo;

    public function ImprimeExtrato()
    {
        echo "Conta: ". $this->tipoDaConta. " Agência: ". $this->agencia. " Conta: ". $this->conta. " Saldo: ". $this->saldo . ' Extrato : ' . $this->calculaSaldo();
    }

     public function deposito(float $valor)
     {
         //$this->saldo = $this->saldo + $valor;

         if ($valor > 0) {
             $this-> saldo += $valor;
             echo "Deposito realizado com sucesso <br>";
         } else {
             echo "Deposito Inválido! <br>";
         }
     }

     public function saque(float $valor)
     {
         //$this->saldo = $this->saldo - $valor;

         if ($this-> saldo >= $valor) {
             $this-> saldo -= $valor;
             echo "Saque realizado com sucesso <br>";
         } else {
             echo "Saldo Insuficiente! <br>";
         }
     }

     abstract public function calculaSaldo();
}

    class Poupanca extends Conta
    {
        public $reajuste;

        public function __construct(string $agencia, string $conta, float $reajuste)
        {
            $this->setTipoDaConta('Poupança');
            $this->agencia= $agencia;
            $this->conta= $conta;
            $this->reajuste = $reajuste;
        }

    public function calculaSaldo()
    {
        return $this->saldo + ($this->saldo * $this->reajuste/100);
    }
    }

class Especial extends Conta
{
    public $saldoEspecial;

    public function __construct(string $agencia, string $conta, float $saldoEspecial)
    {
        $this->setTipoDaConta('Especial');
        $this->agencia= $agencia;
        $this->conta= $conta;
        $this->saldoEspecial = $saldoEspecial;
    }

    public function calculaSaldo()
    {
        return $this->saldo + $this->saldoEspecial;
    }
}

$ctaPoup= new Poupanca('0002-7', '85588-88', 0.54);
//$ctaPoup-> saldo = -1500; - Não pode acessar atributo protegido
$ctaPoup-> deposito(-1500);
$ctaPoup-> saque(3000);
$ctaPoup-> ImprimeExtrato();

echo'<br>';

$ctaEspecial = new Especial('0055-2', '88', 0.54);
$ctaEspecial-> deposito(1500);
$ctaEspecial-> ImprimeExtrato();
