<?php
    abstract class Documento{

        protected $numero;

        abstract public function eValido();

        abstract public function formata();

        public function setNumero($numero){
            $this-> numero = preg_replace( '/[^0-9]/','', $numero); //expressão regular
        }

        public function getNumero(){
            return $this-> numero;
        }

        public function calculaModulo11($qtNumeros, $peso){
            $digito = 0;
            $somatorio = 0;
            $documento = substr($this-> getNumero(),0,$qtNumeros);

            for( $index = 0; $index < $qtNumeros; $index ++){
                $somatorio += $documento[$index] * $peso--;

            }

            $modulo11= $somatorio % 11;

            $digito = 11 - $modulo11;

            if( $digito > 9 ){
                $digito = 0;
            }

            return $digito;

        }

    }

    class CPF extends Documento{

        public function __construct( $numero ){            
            $this-> setNumero($numero);
        }

        public function eValido(){

            $digitoX = $this->calculaModulo11(9,10);
            $digitoY = $this->calculaModulo11(10,11);

            $CPFCalculado = substr($this-> getNumero(),0,9);

            $CPFCalculado = $CPFCalculado . $digitoX . $digitoY;

            if($this-> getNumero() == $CPFCalculado){
                return true;
            }else{return false;
            }

        }

        public function formata(){
            // Formato do CPF: ###.###.###-##
            return substr( $this-> numero, 0, 3) . '.' .
                   substr( $this-> numero, 3, 3) . '.' .
                   substr( $this-> numero, 6, 3) . '-' .
                   substr( $this-> numero, 9, 2);
        }

    }

    class CNPJ extends Documento{

        public function __construct($numero){
            $this->setNumero( $numero );
        }
        
        public function eValido(){

        }

        public function formata(){
            
        }

    }

    class CNH extends Documento{
        
        private $categoria;

        public function __construct($numero, $categoria)
        {
            $this-> setNumero($numero);
            $this-> categoria = $categoria;
        }

        public function eValido(){

        }

        public function formata(){
            
        }

        public function setCategoria($categoria){
            $this-> categoria = $categoria;
        }

        public function getCategoria(){
            return $this-> categoria;
        }

    }

    $cpfMatheus = new CPF("501.479.628-11");
    if($cpfMatheus-> eValido()){
        echo "CPF Válido";
    }else{
        echo "CPF Inválido";
    }

    echo "<br/>";

    $cpfJuju = new CPF("518.237.368-66");
    if($cpfJuju-> eValido()){
        echo "CPF Válido";
    }else{
        echo "CPF Inválido";
    }

?>